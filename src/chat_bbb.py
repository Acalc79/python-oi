from collections import defaultdict


class SuffixData:
    def __init__(self):
        self.most_common = "a"
        self.most_common_freq = 0
        self.frequencies = defaultdict(lambda: 0)

    def update(self, new_letter):
        self.frequencies[new_letter] += 1
        new_letter_freq = self.frequencies[new_letter]
        if new_letter_freq >= self.most_common_freq:
            if new_letter < self.most_common or new_letter_freq > self.most_common_freq:
                self.most_common = new_letter
                self.most_common_freq = new_letter_freq


def main(debug=False):
    n, k, a, b = map(int, input().split(" "))
    string = input()
    suffix_frequencies = defaultdict(lambda: SuffixData())
    for i in range(k, len(string)):
        suffix = string[i - k:i]
        after = string[i]
        suffix_frequencies[suffix].update(after)
    cycle_length, cycle, generated, offset = detect_cycle(suffix_frequencies, string[-k:])
    a = a - 1 # indices start at 0
    if a >= offset + n:
        start_index = (a - (offset + n)) % cycle_length
        end_index = (b - (offset + n)) % cycle_length
        if end_index == 0:
            end_index = cycle_length
        if b - a > cycle_length or start_index >= end_index:
            repcount = (b - a) // cycle_length
            if start_index <= end_index:
                # one full repetition is between start_index and end_index
                repcount -= 1
            result = cycle[start_index:] + cycle * repcount + cycle[:end_index]
            if debug:
                print(f"Case 0: {n}, {offset}, {a}, {b}, {len(result)} [{((b - a - 1) // cycle_length)} repts] vs {b-a}")
                print(f"\t in cycle of length {len(cycle)} [{start_index} - {end_index}], "
                      f"predicted length: {len(cycle[start_index:]) + len()}")
        else:
            result = cycle_length[start_index:end_index]
            if debug:
                print(f"Case 1: {n}, {offset}, {a}, {b}")
    elif b <= offset + n:
        result = generated[a - n:b - n]
        if debug:
            print(f"Case 2: {n}, {offset}, {a}, {b}")
    else:
        end_index = (b - (offset + n)) % cycle_length
        result = generated[a - n:] + cycle * ((b - (offset + n)) // cycle_length) + cycle[:end_index]
        if debug:
            print(f"Case 3: {n}, {offset}, {a}, {b}")
    print(result)


def detect_cycle(suffix_frequencies: dict[str, SuffixData], suffix: str):
    generated = ""
    seen_before = {}
    current = 0
    while True:
        if suffix in seen_before:
            cycle_length = current - seen_before[suffix]
            return cycle_length, generated[-cycle_length:], generated, current
        seen_before[suffix] = current
        next_char = suffix_frequencies[suffix].most_common
        suffix = suffix[1:] + next_char
        generated += next_char
        current += 1


def simulate_full(suffix_frequencies: dict[str, SuffixData], string: str, k: int, length: int):
    suffix = string[-k:]
    for i in range(length):
        next_char = suffix_frequencies[suffix].most_common
        string += next_char
        suffix = suffix[1:] + next_char
    return string


main()
