import math
from collections import Counter

n = int(input())
zeznania = input().split(" ")
counter = Counter(zeznania)


def wrong_distances(zeznania):
    widziane_wczesniej = {}
    for i in range(len(zeznania)):
        if zeznania[i] in widziane_wczesniej:
            if i - widziane_wczesniej[zeznania[i]] != 2:
                return True
        else:
            widziane_wczesniej[zeznania[i]] = i
    return False


if wrong_distances(zeznania):
    print("0")
elif n == len(counter):
    # there are no repeats
    if n % 2 == 1:
        print("0")
    else:
        print("1")
elif any(value > 2 for value in counter.values()):
    print("0")
else:
    # some things are repeated
    # the first hat is definitely unknown
    current_unknown_hat_sequence_size = 1
    saved_unknown_hat_sequences = []
    for i in range(2, n-1, 2):
        # goes through all the even indices
        if zeznania[i-1] == zeznania[i+1]:
            saved_unknown_hat_sequences.append(current_unknown_hat_sequence_size)
            current_unknown_hat_sequence_size = 0
        else:
            current_unknown_hat_sequence_size += 1
    if (n-1) % 2 == 1:
        # ending has just one possibility for how it's filled
        # so no extra work needed
        pass
    else:
        saved_unknown_hat_sequences.append(current_unknown_hat_sequence_size + 1)
    combinations = 1
    for element in saved_unknown_hat_sequences:
        combinations *= element
    unknown_hats_to_place = len(saved_unknown_hat_sequences)
    # all odd indices
    first_unknown_index = n
    for i in range(1, n-1, 2): # 1, 3, 5, ..., aż do n, bez n
        if zeznania[i-1] == zeznania[i+1]:
            first_unknown_index = i + 2
            break
    if first_unknown_index < n:
        current_unknown_hat_sequence_size = 1
        saved_unknown_hat_sequences = []
        for i in range(first_unknown_index+2, n, 2):
            # goes through all the even indices
            if zeznania[i-1] == zeznania[i+1]:
                saved_unknown_hat_sequences.append(current_unknown_hat_sequence_size)
                current_unknown_hat_sequence_size = 0
            else:
                current_unknown_hat_sequence_size += 1
        if n % 2 == 0:
            # only for even n the ending has some unknown hat
            saved_unknown_hat_sequences.append(current_unknown_hat_sequence_size)
        for element in saved_unknown_hat_sequences:
            combinations *= element
        unknown_hats_to_place += len(saved_unknown_hat_sequences)
    print(combinations * math.factorial(unknown_hats_to_place))
