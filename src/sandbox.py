from functools import reduce


fib = lambda n: reduce(lambda x, y: (x[1], sum(x)), range(n-2), (1, 1))[1]
