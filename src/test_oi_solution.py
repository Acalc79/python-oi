import subprocess
import sys
from pathlib import Path


def do_one_test(program_name, file):
    file_result = file.with_suffix(".result")
    file_expected = file.with_suffix(".out")
    with open(file, "r", encoding="utf-8") as infile, \
            open(file_result, "w", encoding="utf-8") as outfile:
        subprocess.run(["python", program_name], stdin=infile, stdout=outfile)
    if file_expected.read_text() == file_result.read_text():
        print(f"Test for file {file.stem}: PASSED!")
        return True
    else:
        print(f"Test for file {file.stem}: FAILED!", file=sys.stderr)
        expected = file_expected.read_text()
        if len(expected) < 200:
            print(f"Expected output: {expected}")
        else:
            print(f"Expected output: {expected[:200]}... [{len(expected)} characters]")
        obtained = file_result.read_text()
        if len(obtained) < 200:
            print(f"Obtained output: {obtained}")
        else:
            print(f"Obtained output: {obtained[:200]}... [{len(obtained)} characters]")
        return False


def do_tests(program_name, directory, quick_fail=False):
    count = 0
    failures = 0
    for file in directory.iterdir():
        if file.suffix == ".in":
            count += 1
            result = do_one_test(program_name, file)
            if not result:
                failures += 1
                if quick_fail:
                    print(f"The first {count - 1} tests passed. First unsuccessful: {file.stem}")
                    return
    print(f"The total of {count} tests finished. There were {failures} failures (failure rate {failures/count}%)")


test_dir = Path("../cza_tests")
program = "chat_bbb.py"
# do_one_test(program, test_dir / "cza2b.in")
do_tests(program, test_dir, True)
