import filecmp
import sys
from collections import defaultdict
from functools import reduce
from operator import mul
from pathlib import Path


def factorial(n):
    return reduce(mul, range(1, n+1), 1)


def combinations(full, debug=False):
    if debug:
        print(f"Analysing full example {full}", file=sys.stderr)
    places = defaultdict(lambda: [])
    for i, elem in enumerate(full):
        if len(places[elem]) > 1:
            return 0
        elif len(places[elem]) == 1 and places[elem][0] != i - 2:
            return 0
        else:
            places[elem].append(i)

    if len(full) % 2 == 1:
        c1, free1 = analyse(full[0::2], True, True, debug)
        c2, free2 = analyse(full[1::2], False, False, debug)
    else:
        c1, free1 = analyse(full[0::2], True, False, debug)
        c2, free2 = analyse(full[1::2], False, True, debug)
    return c1 * c2 * factorial(free1 + free2)


def analyse(sayings, is_front_fixed, is_end_fixed, debug=False):
    if debug:
        fix_str = lambda x: "fixed" if x else "free"
        print(f"Analysing {sayings} with ends: {fix_str(is_front_fixed), fix_str(is_end_fixed)}", file=sys.stderr)
    if is_front_fixed:
        next_is = sayings[0]
        i = 1
        while i < len(sayings) and sayings[i] != next_is:
            next_is = sayings[i]
            i += 1
        if i == len(sayings):
            if is_end_fixed:
                return 0, 0  # impossible, we used up a position that's unavailable
            else:
                return 1, 0  # precisely one combination, with no free spaces
        elif i+1 == len(sayings):
            if is_end_fixed:
                return 1, 0
            else:
                return 1, 1
        # after the expected one we don't know which the next number is referring to, so it's not fixed
        sayings = sayings[i+1:]
    if is_end_fixed:
        prev_is = sayings[-1]
        i = len(sayings) - 2
        while i >= 0 and sayings[i] != prev_is:
            prev_is = sayings[i]
            i -= 1
            if i == 0:
                if prev_is == sayings[i]:
                    return 1, 1
                else:
                    return 1, 0
        # after the expected one we don't know which the next number is referring to, so it's not fixed
        sayings = sayings[:i]
    if debug:
        print(f"Free ended: {sayings}", file=sys.stderr)
    if not sayings:
        return 1, 0

    combs = 1
    frees = 0
    fragment_start = 0
    for i, (x, y) in enumerate(zip(sayings, sayings[1:])):
        if x == y:
            if debug:
                print(f"Found definite place at {i} with value {x}", file=sys.stderr)
            combs *= i + 1 - fragment_start # TODO: verify empirically
            frees += 1
            fragment_start = i + 2
    combs *= len(sayings) + 1 - fragment_start
    frees += 1
    if debug:
        print(f"Result: {combs, frees}", file=sys.stderr)
    return combs, frees


def main(debug=False):
    num = int(input())
    sayings = [int(number) for number in input().split()]
    assert len(sayings) == num
    print(combinations(sayings, debug) % (10**9 + 7))


def do_tests(directory, quickfail=False):
    count = 0
    failures = 0
    stdin = sys.stdin
    stdout = sys.stdout
    for file in directory.iterdir():
        if file.suffix == ".in":
            count += 1
            file_result = file.with_suffix(".result")
            file_expected = file.with_suffix(".out")
            with open(file, "r", encoding="utf-8") as infile, \
                    open(file_result, "w", encoding="utf-8") as outfile:
                sys.stdin = infile
                sys.stdout = outfile
                main()
                sys.stdin = stdin
                sys.stdout = stdout
            if file_expected.read_text() == file_result.read_text():
                print(f"Test for file {file.stem}: PASSED!")
            else:
                print(f"Test for file {file.stem}: FAILED!", file=sys.stderr)
                print(f"Expected output: {file_expected.read_text()}")
                print(f"Obtained output: {file_result.read_text()}")
                failures += 1
                if quickfail:
                    print(f"The first {count - 1} tests passed.")
                    return
    print(f"The total of {count} tests finished. There were {failures} failures (failure rate {failures/count}%)")


testdir = Path("../imp_tests")
