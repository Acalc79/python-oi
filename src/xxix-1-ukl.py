from itertools import permutations

import graphviz as gv


def prepare_graph(n, engine="dot"):
    graph = gv.Digraph(engine=engine, format="pdf")
    for perm in permutations(range(1, n+1)):
        if any(perm[i] > perm[i+1] for i in range(len(perm) - 1)):
            find = perm[0] - 1 if perm[0] > 1 else len(perm)
            i = perm.index(find)
            after = (find,) + perm[:i] + perm[i+1:]
            graph.edge("".join(map(str, perm)), "".join(map(str, after)), label=f"{i}")
    graph.render(f"ukladanie-{n}-{engine}")


def test():
    for i in range(2, 7):
        prepare_graph(i, "dot")

W = [0, 1]
S = [0, 0]
P = [0, W[1]]
R = [0, S[1]]

def solve(n, m):
    for k in range(2, n-2):
        W.append(1 + P[k-1] + (n-k) * W[k-1])
        S.append()
        P.append(P[k-1] + W[k])
        R.append(R[k-1] + S[k])
    S_prime = 0
    for w in range(1, n-1):
        S_prime = (S_prime + P[w] + S[w]) % m
    S_bis = (n-1) * (n-1) * n // 2 * (1 + P[n-2])
