from enum import Enum
from dataclasses import dataclass, field
from typing import Tuple, MutableMapping, MutableSequence


@dataclass
class Pos:
    y: int
    x: int


Colour = int


@dataclass
class Check:
    w: int
    k: int


class Direction(Enum):
    G = "G"
    D = "D"
    L = "L"
    P = "P"

    def delta(self) -> Tuple[int, int]:
        match self:
            case Direction.D:
                return 0, 1
            case Direction.G:
                return 0, -1
            case Direction.P:
                return 1, 0
            case Direction.L:
                return -1, 0


Instruction = Direction | Check


@dataclass
class ProgramState:
    snacks: MutableMapping[Pos, Colour]
    current_pos: Pos = 1, 1
    move_count: int = 0
    pos: MutableMapping[Pos, int] = field(default_factory=lambda: {(1, 1): 0})
    cols: MutableSequence[Colour] = field(default_factory=lambda: [0])


def process(string: str) -> Instruction:
    if string in "GDLP":
        return Direction(string)
    return Check(*map(int, string.split(" ")))


def target(current_pos: Pos, direction: Direction) -> Pos:
    x, y = current_pos
    dx, dy = direction.delta()
    return Pos(x=x + dx, y=y + dy)


def move_to(target_pos: Pos, state: ProgramState) -> None:
    state.current_pos = target_pos
    state.move_count += 1
    state.pos[target_pos] = state.move_count
    if target_pos in state.snacks:
        state.cols.append(state.snacks[target_pos])
        del state.snacks[target_pos]


def output(pos: Pos, state: ProgramState) -> int:
    if pos not in state.pos:
        return -1
    index: int = state.move_count - state.pos[pos]
    if index < len(state.cols):
        return state.cols[index]
    return -1


def main():
    m, n, p = map(int, input().split(" "))  # type: int, int, int
    state: ProgramState = ProgramState({(w, k): c for _ in range(p) for w, k, c in map(int, input().split(" "))})
    for _ in range(n):
        instruction: Instruction = process(input())
        match instruction:
            case Direction():
                move_to(target(state.current_pos, instruction), state)
            case Check(w, k):
                print(output(Pos(w, k), state))

# ideas:
# keep track of all positions in reverse order [O(n) space, O(1) time] - pos array
# keep track of all colours in reverse order [O(n) space, O(1) time] - colour array
# modify snacks dict when eaten
# O(1) lookup of positions using hash map from position to the last index in pos that it appeared in
